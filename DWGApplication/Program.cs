﻿using DWGApplication.Source;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWGApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            Dwg_Data dwg_data = new Dwg_Data();
            dwgClass dwg_obj = new dwgClass();
            string filename = args[4];
            int error = dwg_obj.dwg_read_file(filename, ref dwg_data);

            if(error != 0)
            {
                Bit_Chain dat = new Bit_Chain();
                string outfile = args[3];
                FileStream fw = null;
                if (!String.IsNullOrEmpty(outfile))
                {
                    dat.fh = new FileStream(outfile, FileMode.Append);
                }

                //fprintf(stderr, "\n");
                dat.version = dat.from_version = dwg_data.header.version;
                Source.out_json out_json_obj = new Source.out_json();
                error = out_json_obj.dwg_write_json(ref dat, ref dwg_data);
            }
            else
            {
                Console.WriteLine("Unable to export dwg json file!!!!!!!!!!!");
            }
        }
    }
}
