﻿using DWGApplication.Source;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWGApplication
{
    //const int DWG_OPTS_LOGLEVEL = 0xf;
    
    public enum DWG_ERROR
    {
        DWG_NOERR = 0,
        /* sorted by severity */
        DWG_ERR_WRONGCRC = 1,
        DWG_ERR_NOTYETSUPPORTED = 1 << 1, /* 2 */
        DWG_ERR_UNHANDLEDCLASS = 1 << 2, /* 4 */
        DWG_ERR_INVALIDTYPE = 1 << 3, /* 8 */
        DWG_ERR_INVALIDHANDLE = 1 << 4, /* 16 */
        DWG_ERR_INVALIDEED = 1 << 5, /* 32 */
        DWG_ERR_VALUEOUTOFBOUNDS = 1 << 6, /* 64 */
                                           /* -------- critical errors ------- */
        DWG_ERR_CLASSESNOTFOUND = 1 << 7, /* 128 */
        DWG_ERR_SECTIONNOTFOUND = 1 << 8, /* 256 */
        DWG_ERR_PAGENOTFOUND = 1 << 9, /* 512 */
        DWG_ERR_INTERNALERROR = 1 << 10,/* 1024 */
        DWG_ERR_INVALIDDWG = 1 << 11,/* 2048 */
        DWG_ERR_IOERROR = 1 << 12,/* 4096 */
        DWG_ERR_OUTOFMEM = 1 << 13,/* 8192 */

    };

    public enum DWG_VERSION_TYPE
    {
        R_INVALID,
        R_1_1,  /* MC0.0  MicroCAD Release 1.1 */
        R_1_2,  /* AC1.2  AutoCAD Release 1.2 */
        R_1_4,  /* AC1.4  AutoCAD Release 1.4 */
        R_2_0,  /* AC1.50 AutoCAD Release 2.0 */
        R_2_1,  /* AC2.10 AutoCAD Release 2.10 */
        R_2_5,  /* AC1002 AutoCAD Release 2.5 */
        R_2_6,  /* AC1003 AutoCAD Release 2.6 */
        R_9,        /* AC1004 AutoCAD Release 9 */
        R_10,       /* AC1006 AutoCAD Release 10 */
        R_11,       /* AC1009 AutoCAD Release 11/12 (LT R1/R2) */
        R_13,       /* AC1012 AutoCAD Release 13 */
        R_14,       /* AC1014 AutoCAD Release 14 */
        R_2000, /* AC1015 AutoCAD Release 2000 */
        R_2004, /* AC1018 AutoCAD Release 2004 */
        R_2007, /* AC1021 AutoCAD Release 2007 */
        R_2010, /* AC1024 AutoCAD Release 2010 */
        R_2013, /* AC1027 AutoCAD Release 2013 */
        R_2018, /* AC1032 AutoCAD Release 2018 */
        R_AFTER
    };

    public struct _dwg_section
    {
        Int32 number; /* preR13: count of entries, r2007: id */
        UInt32 size;   /* now unsigned */
        UInt64 address;
        UInt32 parent;
        UInt32 left;
        UInt32 right;
        UInt32 x00;
        DWG_VERSION_TYPE type; /* to be casted to Dwg_Section_Type_r11 preR13 */
                               /* => section_info? */
        char[] name; // new char[64];
        /*!< r2004 section fields: */
        UInt32 section_type;
        UInt32 decomp_data_size;
        UInt32 comp_data_size;
        UInt32 compression_type;
        UInt32 checksum;
    };

    public struct Dwg_Section_InfoHdr
    {
        public UInt32 num_desc;
        public UInt32 compressed; /* Compressed (1 = no, 2 = yes, normally 2) */
        public UInt32 max_size;
        public UInt32 encrypted; /* (0 = no, 1 = yes, 2 = unknown) */
        public UInt32 num_desc2;
    };

    public struct Dwg_Section_Info
    {
        Int64 size;
        UInt32 num_sections;
        UInt32 max_decomp_size;
        UInt32 unknown;
        UInt32 compressed; /* Compressed (1 = no, 2 = yes, normally 2) */
        UInt32 type;       /* The dynamic index as read/written */
        UInt32 encrypted;  /* (0 = no, 1 = yes, 2 = unknown) */
        char[] name; //= new char[64];
        DWG_VERSION_TYPE fixedtype;  /* to search for */
        _dwg_section sections;
    };

    public struct Dwg_Class
    {
        UInt16 number;
        UInt16 proxyflag; 
        string appname;
        string cppname;
        string dxfname; /*!< ASCII or UTF-8 */
        string dxfname_u; /*!< r2007+, always transformed to dxfname as UTF-8 */
        char is_zombie; /*!< i.e. was_proxy, not loaded class */
        UInt16 item_class_id; /*!< really is_entity. 1f2 for entities, 1f3 for objects */
        UInt32 num_instances; /*!< 91 instance count for a custom class */
        UInt32 dwg_version;
        UInt32 maint_version;
        UInt32 unknown_1; /*!< def: 0L */
        UInt32 unknown_2; /*!< def: 0L */
    };

    public enum DWG_OBJECT_TYPE
    {
        DWG_TYPE_UNUSED = 0x00,
        DWG_TYPE_TEXT = 0x01,
        DWG_TYPE_ATTRIB = 0x02,
        DWG_TYPE_ATTDEF = 0x03,
        DWG_TYPE_BLOCK = 0x04,
        DWG_TYPE_ENDBLK = 0x05,
        DWG_TYPE_SEQEND = 0x06,
        DWG_TYPE_INSERT = 0x07,
        DWG_TYPE_MINSERT = 0x08,
        /* DWG_TYPE_<UNKNOWN> = 0x09, */
        DWG_TYPE_VERTEX_2D = 0x0a,
        DWG_TYPE_VERTEX_3D = 0x0b,
        DWG_TYPE_VERTEX_MESH = 0x0c,
        DWG_TYPE_VERTEX_PFACE = 0x0d,
        DWG_TYPE_VERTEX_PFACE_FACE = 0x0e,
        DWG_TYPE_POLYLINE_2D = 0x0f,
        DWG_TYPE_POLYLINE_3D = 0x10,
        DWG_TYPE_ARC = 0x11,
        DWG_TYPE_CIRCLE = 0x12,
        DWG_TYPE_LINE = 0x13,
        DWG_TYPE_DIMENSION_ORDINATE = 0x14,
        DWG_TYPE_DIMENSION_LINEAR = 0x15,
        DWG_TYPE_DIMENSION_ALIGNED = 0x16,
        DWG_TYPE_DIMENSION_ANG3PT = 0x17,
        DWG_TYPE_DIMENSION_ANG2LN = 0x18,
        DWG_TYPE_DIMENSION_RADIUS = 0x19,
        DWG_TYPE_DIMENSION_DIAMETER = 0x1A,
        DWG_TYPE_POINT = 0x1b,
        DWG_TYPE__3DFACE = 0x1c,
        DWG_TYPE_POLYLINE_PFACE = 0x1d,
        DWG_TYPE_POLYLINE_MESH = 0x1e,
        DWG_TYPE_SOLID = 0x1f,
        DWG_TYPE_TRACE = 0x20,
        DWG_TYPE_SHAPE = 0x21,
        DWG_TYPE_VIEWPORT = 0x22,
        DWG_TYPE_ELLIPSE = 0x23,
        DWG_TYPE_SPLINE = 0x24,
        DWG_TYPE_REGION = 0x25,
        DWG_TYPE__3DSOLID = 0x26,
        DWG_TYPE_BODY = 0x27,
        DWG_TYPE_RAY = 0x28,
        DWG_TYPE_XLINE = 0x29,
        DWG_TYPE_DICTIONARY = 0x2a,
        DWG_TYPE_OLEFRAME = 0x2b,
        DWG_TYPE_MTEXT = 0x2c,
        DWG_TYPE_LEADER = 0x2d,
        DWG_TYPE_TOLERANCE = 0x2e,
        DWG_TYPE_MLINE = 0x2f,
        DWG_TYPE_BLOCK_CONTROL = 0x30,
        DWG_TYPE_BLOCK_HEADER = 0x31,
        DWG_TYPE_LAYER_CONTROL = 0x32,
        DWG_TYPE_LAYER = 0x33,
        DWG_TYPE_STYLE_CONTROL = 0x34,
        DWG_TYPE_STYLE = 0x35,
        /* DWG_TYPE_<UNKNOWN> = 0x36, */
        /* DWG_TYPE_<UNKNOWN> = 0x37, */
        DWG_TYPE_LTYPE_CONTROL = 0x38,
        DWG_TYPE_LTYPE = 0x39,
        /* DWG_TYPE_<UNKNOWN> = 0x3a, */
        /* DWG_TYPE_<UNKNOWN> = 0x3b, */
        DWG_TYPE_VIEW_CONTROL = 0x3c,
        DWG_TYPE_VIEW = 0x3d,
        DWG_TYPE_UCS_CONTROL = 0x3e,
        DWG_TYPE_UCS = 0x3f,
        DWG_TYPE_VPORT_CONTROL = 0x40,
        DWG_TYPE_VPORT = 0x41,
        DWG_TYPE_APPID_CONTROL = 0x42,
        DWG_TYPE_APPID = 0x43,
        DWG_TYPE_DIMSTYLE_CONTROL = 0x44,
        DWG_TYPE_DIMSTYLE = 0x45,
        DWG_TYPE_VPORT_ENTITY_CONTROL = 0x46,
        DWG_TYPE_VPORT_ENTITY_HEADER = 0x47,
        DWG_TYPE_GROUP = 0x48,
        DWG_TYPE_MLINESTYLE = 0x49,
        DWG_TYPE_OLE2FRAME = 0x4a,
        DWG_TYPE_DUMMY = 0x4b,
        DWG_TYPE_LONG_TRANSACTION = 0x4c,
        DWG_TYPE_LWPOLYLINE = 0x4d, /* ?? */
        DWG_TYPE_HATCH = 0x4e,
        DWG_TYPE_XRECORD = 0x4f,
        DWG_TYPE_PLACEHOLDER = 0x50,
        DWG_TYPE_VBA_PROJECT = 0x51,
        DWG_TYPE_LAYOUT = 0x52,

        DWG_TYPE_PROXY_ENTITY = 0x1f2, /* 498 */
        DWG_TYPE_PROXY_OBJECT = 0x1f3, /* 499 */

        /* non-fixed types > 500. not stored as type, but as fixedtype */

        DWG_TYPE_ACDSRECORD = 0x1ff + 1,
        DWG_TYPE_ACDSSCHEMA,
        DWG_TYPE_ACMECOMMANDHISTORY,
        DWG_TYPE_ACMESCOPE,
        DWG_TYPE_ACMESTATEMGR,
        DWG_TYPE_ACSH_BOX_CLASS,
        DWG_TYPE_ACSH_EXTRUSION_CLASS,
        DWG_TYPE_ACSH_HISTORY_CLASS,
        DWG_TYPE_ACSH_PYRAMID_CLASS,
        DWG_TYPE_ACSH_REVOLVE_CLASS,
        DWG_TYPE_ACSH_SPHERE_CLASS,
        DWG_TYPE_ACSH_SWEEP_CLASS,
        DWG_TYPE_ALDIMOBJECTCONTEXTDATA,
        DWG_TYPE_ARC_DIMENSION,
        DWG_TYPE_ASSOC2DCONSTRAINTGROUP,
        DWG_TYPE_ASSOCACTION,
        DWG_TYPE_ASSOCALIGNEDDIMACTIONBODY,
        DWG_TYPE_ASSOCDEPENDENCY,
        DWG_TYPE_ASSOCEXTRUDEDSURFACEACTIONBODY,
        DWG_TYPE_ASSOCGEOMDEPENDENCY,
        DWG_TYPE_ASSOCLOFTEDSURFACEACTIONBODY,
        DWG_TYPE_ASSOCNETWORK,
        DWG_TYPE_ASSOCOSNAPPOINTREFACTIONPARAM,
        DWG_TYPE_ASSOCPERSSUBENTMANAGER,
        DWG_TYPE_ASSOCPLANESURFACEACTIONBODY,
        DWG_TYPE_ASSOCREVOLVEDSURFACEACTIONBODY,
        DWG_TYPE_ASSOCSWEPTSURFACEACTIONBODY,
        DWG_TYPE_ASSOCVERTEXACTIONPARAM,
        DWG_TYPE_ATEXT,
        DWG_TYPE_BACKGROUND,
        DWG_TYPE_BLKREFOBJECTCONTEXTDATA,
        DWG_TYPE_BLOCKGRIPLOCATIONCOMPONENT,
        DWG_TYPE_BLOCKVISIBILITYPARAMETER,
        DWG_TYPE_BLOCKVISIBILITYGRIP,
        DWG_TYPE_CAMERA,
        DWG_TYPE_CELLSTYLEMAP,
        DWG_TYPE_CSACDOCUMENTOPTIONS,
        DWG_TYPE_CURVEPATH,
        DWG_TYPE_DATALINK,
        DWG_TYPE_DATATABLE,
        DWG_TYPE_DBCOLOR,
        DWG_TYPE_DETAILVIEWSTYLE,
        DWG_TYPE_DICTIONARYVAR,
        DWG_TYPE_DICTIONARYWDFLT,
        DWG_TYPE_DIMASSOC,
        DWG_TYPE_DYNAMICBLOCKPURGEPREVENTER,
        DWG_TYPE_EVALUATION_GRAPH,
        DWG_TYPE_EXTRUDEDSURFACE,
        DWG_TYPE_FIELD,
        DWG_TYPE_FIELDLIST,
        DWG_TYPE_GEODATA,
        DWG_TYPE_GEOMAPIMAGE,
        DWG_TYPE_GEOPOSITIONMARKER,
        DWG_TYPE_HELIX,
        DWG_TYPE_IDBUFFER,
        DWG_TYPE_IMAGE,
        DWG_TYPE_IMAGEDEF,
        DWG_TYPE_IMAGEDEF_REACTOR,
        DWG_TYPE_LAYER_INDEX,
        DWG_TYPE_LAYERFILTER,
        DWG_TYPE_LAYOUTPRINTCONFIG,
        DWG_TYPE_LEADEROBJECTCONTEXTDATA,
        DWG_TYPE_LIGHT,
        DWG_TYPE_LIGHTLIST,
        DWG_TYPE_LOFTEDSURFACE,
        DWG_TYPE_MATERIAL,
        DWG_TYPE_MENTALRAYRENDERSETTINGS,
        DWG_TYPE_MESH,
        DWG_TYPE_MLEADEROBJECTCONTEXTDATA,
        DWG_TYPE_MLEADERSTYLE,
        DWG_TYPE_MOTIONPATH,
        DWG_TYPE_MTEXTATTRIBUTEOBJECTCONTEXTDATA,
        DWG_TYPE_MTEXTOBJECTCONTEXTDATA,
        DWG_TYPE_MULTILEADER,
        DWG_TYPE_NAVISWORKSMODEL,
        DWG_TYPE_NAVISWORKSMODELDEF,
        DWG_TYPE_NPOCOLLECTION,
        DWG_TYPE_NURBSURFACE,
        DWG_TYPE_OBJECT_PTR,
        DWG_TYPE_PERSUBENTMGR,
        DWG_TYPE_PLANESURFACE,
        DWG_TYPE_PLOTSETTINGS,
        DWG_TYPE_POINTCLOUD,
        DWG_TYPE_POINTPATH,
        DWG_TYPE_RAPIDRTRENDERENVIRONMENT,
        DWG_TYPE_RAPIDRTRENDERSETTINGS,
        DWG_TYPE_RASTERVARIABLES,
        DWG_TYPE_RENDERENVIRONMENT,
        DWG_TYPE_RENDERENTRY,
        DWG_TYPE_RENDERGLOBAL,
        DWG_TYPE_RENDERSETTINGS,
        DWG_TYPE_REVOLVEDSURFACE,
        DWG_TYPE_RTEXT,
        DWG_TYPE_SCALE,
        DWG_TYPE_SECTIONOBJECT,
        DWG_TYPE_SECTIONVIEWSTYLE,
        DWG_TYPE_SECTION_MANAGER,
        DWG_TYPE_SECTION_SETTINGS,
        DWG_TYPE_SORTENTSTABLE,
        DWG_TYPE_SPATIAL_FILTER,
        DWG_TYPE_SPATIAL_INDEX,
        DWG_TYPE_SUN,
        DWG_TYPE_SUNSTUDY,
        DWG_TYPE_SWEPTSURFACE,
        DWG_TYPE_TABLE,
        DWG_TYPE_TABLECONTENT,
        DWG_TYPE_TABLEGEOMETRY,
        DWG_TYPE_TABLESTYLE,
        DWG_TYPE_TEXTOBJECTCONTEXTDATA,
        DWG_TYPE_TVDEVICEPROPERTIES,
        DWG_TYPE_UNDERLAY, /* not separate DGN,DWF,PDF types */
        DWG_TYPE_UNDERLAYDEFINITION, /* not separate DGN,DWF,PDF types */
        DWG_TYPE_VISIBILITYGRIPENTITY,
        DWG_TYPE_VISIBILITYPARAMETERENTITY,
        DWG_TYPE_VISUALSTYLE,
        DWG_TYPE_WIPEOUT,
        DWG_TYPE_WIPEOUTVARIABLES,
        DWG_TYPE_XREFPANELOBJECT,
        // after 1.0 add new types here for binary compat

        DWG_TYPE_FREED = 0xfffd,
        DWG_TYPE_UNKNOWN_ENT = 0xfffe,
        DWG_TYPE_UNKNOWN_OBJ = 0xffff,
    };

    public enum Dwg_Object_Supertype
    {
        DWG_SUPERTYPE_ENTITY, DWG_SUPERTYPE_OBJECT
    };

    public struct Dwg_Handle
    {
        char code; /*!< OFFSETOBJHANDLE if > 6 */
        char size;
        long value;
        char is_global; // to be freed or not
    };

    public struct Dwg_Chain
    {
        public string chain;
        public long size;
        public long longbyte;
        public char bit;
    };

    public struct Dwg_Data
    {        
        public struct Dwg_Header
        {
            public DWG_VERSION_TYPE version;          /* option. set by --as (convert to) */
            public DWG_VERSION_TYPE from_version;     /* calculated from the header magic */
            public byte[] zero_5;// = new char[5];
            public byte is_maint;
            byte zero_one_or_three;
            UInt16[] unknown_s; //= new  UInt16[3]        /* <R13 */
            UInt32 thumbnail_address;    /* THUMBNAIL or AdDb:Preview */
            byte dwg_version;
            byte maint_version;
            UInt16 codepage;
            byte unknown_0;            /* R2004+ */
            byte app_dwg_version;      /* R2004+ */
            byte app_maint_version;    /* R2004+ */
            UInt32 security_type;        /* R2004+ */
            UInt32 rl_1c_address;        /* R2004+ mostly 0 */
            UInt32 summaryinfo_address;  /* R2004+ */
            UInt32 vbaproj_address;      /* R2004+ */
            UInt32 r2004_header_address; /* R2004+ */

            public UInt32 num_sections; // TODO: should not be here
            _dwg_section section;
            public Dwg_Section_InfoHdr section_infohdr; /* R2004+ */
            //Dwg_Section_Info* section_info;
        };

        public Dwg_Header header;

        public UInt16 num_classes;        /*!< number of classes */
        public Dwg_Class dwg_class;         /*!< array of classes */
        public UInt32 num_objects;        /*!< number of objects */
        //Dwg_Object dwg_object;           /*!< list of all objects and entities */
        public UInt32 num_entities;       /*!< number of entities in object */
        public UInt32 num_object_refs;    /*!< number of object_ref's (resolved handles) */
        //Dwg_Object_Ref object_ref;   /*!< array of most handles */
        public dwg_inthash object_map;   /*!< map of all handles */
        public int dirty_refs;                /* 1 if we added an entity, and invalidated all
                                    the internal ref->obj's */
        public int opts;             /* See DWG_OPTS_* below */
        public Dwg_Chain thumbnail;
    }

    public struct Dwg_Bitcode_2BD
    {
        double x;
        double y;
    };

    public struct Dwg_Bitcode_3BD
    {
        double x;
        double y;
        double z;
    };

    public struct Dwg_Entity_TEXT
    {
        Dwg_Object_Entity parent;

        char dataflags;        /*!< r2000+ */
        double elevation;        /*!< DXF 30 (z coord of 10), when dataflags & 1 */
        Dwg_Bitcode_2BD insertion_pt; /*!< DXF 10 */
        Dwg_Bitcode_2BD alignment_pt; /*!< DXF 11. optional, when dataflags & 2, i.e 72/73 != 0 */
        Dwg_Bitcode_3BD extrusion;       /*!< DXF 210. Default 0,0,1 */
        double thickness;       /*!< DXF 39 */
        double oblique_angle;   /*!< DXF 51 */
        double rotation;        /*!< DXF 50 */
        double height;          /*!< DXF 40 */
        double width_factor;    /*!< DXF 41 */
        string text_value;      /*!< DXF 1 */
        UInt16 generation;      /*!< DXF 71 */
        UInt16 horiz_alignment; /*!< DXF 72. options 0-5:
                                 0 = Left; 1= Center; 2 = Right; 3 = Aligned;
                                 4 = Middle; 5 = Fit */
        UInt16 vert_alignment;  /*!< DXF 73. options 0-3:
                                 0 = Baseline; 1 = Bottom; 2 = Middle; 3 = Top */
        Dwg_Object_Ref style;
    };

    public struct Dwg_Entity_ATTRIB
    {
        public Dwg_Object_Entity parent;

        //BITCODE_BD elevation;
        //BITCODE_2DPOINT insertion_pt;
        //BITCODE_2DPOINT alignment_pt;
        //BITCODE_BE extrusion;
        //BITCODE_RD thickness;
        //BITCODE_RD oblique_angle;
        //BITCODE_RD rotation;
        //BITCODE_RD height;
        //BITCODE_RD width_factor;
        //BITCODE_TV text_value;
        //BITCODE_BS generation;
        //BITCODE_BS horiz_alignment;
        //BITCODE_BS vert_alignment;
        //BITCODE_RC dataflags;
        //BITCODE_RC class_version; /* R2010+ */
        //BITCODE_RC type;    /* R2018+ */
        //BITCODE_TV tag;
        //BITCODE_BS field_length; /* DXF 73 but unused */
        //BITCODE_RC flags;
        //BITCODE_B lock_position_flag;
        //BITCODE_H style;
        //BITCODE_H mtext_handles; /* R2018+ TODO */
        //BITCODE_BS annotative_data_size; /* R2018+ */
        //BITCODE_RC annotative_data_bytes;
        //BITCODE_H annotative_app;
        //BITCODE_BS annotative_short;
    };

    public struct Dwg_Object_Entity
    {
        UInt32 objid; /*!< link to the parent */

        public struct tio
        {
            int UNUSED;
            Dwg_Entity_TEXT TEXT;
            Dwg_Entity_ATTRIB ATTRIB;
            //Dwg_Entity_ATTDEF* ATTDEF;
            //Dwg_Entity_BLOCK* BLOCK;
            //Dwg_Entity_ENDBLK* ENDBLK;
            //Dwg_Entity_SEQEND* SEQEND;
            //Dwg_Entity_INSERT* INSERT;
            //Dwg_Entity_MINSERT* MINSERT;
            //Dwg_Entity_VERTEX_2D* VERTEX_2D;
            //Dwg_Entity_VERTEX_3D* VERTEX_3D;
            //Dwg_Entity_VERTEX_MESH* VERTEX_MESH;
            //Dwg_Entity_VERTEX_PFACE* VERTEX_PFACE;
            //Dwg_Entity_VERTEX_PFACE_FACE* VERTEX_PFACE_FACE;
            //Dwg_Entity_POLYLINE_2D* POLYLINE_2D;
            //Dwg_Entity_POLYLINE_3D* POLYLINE_3D;
            //Dwg_Entity_ARC* ARC;
            //Dwg_Entity_ATEXT* ATEXT;
            //Dwg_Entity_CIRCLE* CIRCLE;
            //Dwg_Entity_LINE* LINE;
            //Dwg_DIMENSION_common* DIMENSION_common;
            //Dwg_Entity_DIMENSION_ORDINATE* DIMENSION_ORDINATE;
            //Dwg_Entity_DIMENSION_LINEAR* DIMENSION_LINEAR;
            //Dwg_Entity_DIMENSION_ALIGNED* DIMENSION_ALIGNED;
            //Dwg_Entity_DIMENSION_ANG3PT* DIMENSION_ANG3PT;
            //Dwg_Entity_DIMENSION_ANG2LN* DIMENSION_ANG2LN;
            //Dwg_Entity_DIMENSION_RADIUS* DIMENSION_RADIUS;
            //Dwg_Entity_DIMENSION_DIAMETER* DIMENSION_DIAMETER;
            //Dwg_Entity_POINT* POINT;
            //Dwg_Entity__3DFACE* _3DFACE;
            //Dwg_Entity_POLYLINE_PFACE* POLYLINE_PFACE;
            //Dwg_Entity_POLYLINE_MESH* POLYLINE_MESH;
            //Dwg_Entity_SOLID* SOLID;
            //Dwg_Entity_TRACE* TRACE;
            //Dwg_Entity_SHAPE* SHAPE;
            //Dwg_Entity_VIEWPORT* VIEWPORT;
            //Dwg_Entity_ELLIPSE* ELLIPSE;
            //Dwg_Entity_SPLINE* SPLINE;
            //Dwg_Entity_3DSOLID* _3DSOLID;
            //Dwg_Entity_REGION* REGION;
            //Dwg_Entity_BODY* BODY;
            //Dwg_Entity_RAY* RAY;
            //Dwg_Entity_XLINE* XLINE;
            //Dwg_Entity_OLEFRAME* OLEFRAME;
            //Dwg_Entity_MTEXT* MTEXT;
            //Dwg_Entity_LEADER* LEADER;
            //Dwg_Entity_TOLERANCE* TOLERANCE;
            //Dwg_Entity_MLINE* MLINE;
            //Dwg_Entity_OLE2FRAME* OLE2FRAME;
            //Dwg_Entity_HATCH* HATCH;

            //Dwg_Entity_CAMERA* CAMERA;
            //Dwg_Entity_GEOPOSITIONMARKER* GEOPOSITIONMARKER;
            //Dwg_Entity_HELIX* HELIX;
            //Dwg_Entity_IMAGE* IMAGE;
            //Dwg_Entity_LIGHT* LIGHT;
            //Dwg_Entity_LWPOLYLINE* LWPOLYLINE;
            //Dwg_Entity_MULTILEADER* MULTILEADER;
            //Dwg_Entity_SECTIONOBJECT* SECTIONOBJECT;
            //Dwg_Entity_PROXY_ENTITY* PROXY_ENTITY;
            //Dwg_Entity_PROXY_LWPOLYLINE* PROXY_LWPOLYLINE;
            //Dwg_Entity_NURBSURFACE* NURBSURFACE;
            //Dwg_Entity_PLANESURFACE* PLANESURFACE;
            //Dwg_Entity_EXTRUDEDSURFACE* EXTRUDEDSURFACE;
            //Dwg_Entity_LOFTEDSURFACE* LOFTEDSURFACE;
            //Dwg_Entity_REVOLVEDSURFACE* REVOLVEDSURFACE;
            //Dwg_Entity_SWEPTSURFACE* SWEPTSURFACE;
            //Dwg_Entity_TABLE* TABLE;
            //Dwg_Entity_UNDERLAY* UNDERLAY;
            //Dwg_Entity_WIPEOUT* WIPEOUT;
            //Dwg_Entity_ARC_DIMENSION* ARC_DIMENSION;
            //Dwg_Entity_MESH* MESH;
            //Dwg_Entity_NAVISWORKSMODEL* NAVISWORKSMODEL;
            //Dwg_Entity_RTEXT* RTEXT;

            //Dwg_Entity_UNKNOWN_ENT* UNKNOWN_ENT;
        };

    }

    public struct Dwg_Object
    {
        UInt32 size;       /*!< in bytes */
        long address; /*!< byte offset in the file */
        int type;     /*!< fixed or variable (class - 500) */
        UInt32 index;      /*!< into dwg->object[] */
        DWG_OBJECT_TYPE fixedtype; /*!< into a global list */
        string name;            /*!< our public entity/object name */
        string dxfname;         /*!< the internal dxf classname, often with a ACDB prefix */

        Dwg_Object_Supertype supertype;
        //struct tio
        //{
        //    Dwg_Object_Entity* entity;
        //    Dwg_Object_Object* object;
        //};

        Dwg_Handle handle;
        Dwg_Data parent;
        Dwg_Class klass;          /* the optional class of a variable object */

        UInt32 bitsize;        /* common + object fields, but no handles */
        long bitsize_pos; /* bitsize offset in bits: r13-2007 */
        long hdlpos;      /* relative offset, in bits */
        char was_bitsize_set;    /* internally for encode only */
        char has_strings;        /*!< r2007+ */
        UInt32 stringstream_size;  /*!< r2007+ in bits, unused */
        long handlestream_size; /*!< r2010+ in bits */
        long common_size; /* relative offset from type ... end common_entity_data */

        UInt32 num_unknown_bits;
        string unknown_bits;

    };

    public struct Dwg_Object_Ref
    {
        Dwg_Object obj;
        Dwg_Handle handleref;
        long absolute_ref;
    };

    public struct _stat64
    {
        public int st_dev;
        public short st_ino;
        public short st_mode;
        public short st_nlink;
        public short st_uid;
        public short st_gid;
        public int st_rdev;
        public Int64 st_size;
        public long st_atime;
        public long st_mtime;
        public long st_ctime;
    };
    public class dwgClass
    {
        public const int DWG_OPTS_LOGLEVEL = 0xf;
        public static int loglevel = 0;

        public int dat_read_file(ref Bit_Chain dat, ref byte[] fp,string filename)
        {            
            using (var strream = new BinaryReader(File.OpenRead(filename)))
            {
                fp = new Byte[strream.BaseStream.Length];
                for(int i =0; i<strream.BaseStream.Length;i++)
                {
                    fp[i] = strream.ReadByte();
                }
            }

            dat.chain = fp;

            if (fp.Length < 0)
            {
                Console.WriteLine("Could not read file");
                //fp.Close();
                return (int)DWG_ERROR.DWG_ERR_IOERROR;
            }
            return 0;
        }
              
        public int dwg_read_file(string filename , ref Dwg_Data dwg)
        {
            FileStream fp = null;            
            _stat64 attrib = new _stat64();
            //int size;
            Bit_Chain bit_chain = new Bit_Chain();
            int error = (int)DWG_ERROR.DWG_NOERR;

            loglevel = dwg.opts & 0xf;            
            dwg.opts = loglevel;

            if (Char.Equals(filename, "-"))
            {
                fp = File.Create(Console.ReadLine());
            }
            else
            {
                if (!File.Exists(filename))
                {
                    Console.WriteLine("File not found: %s\n", filename);
                    return (int)DWG_ERROR.DWG_ERR_IOERROR;
                }
                                
                fp = new FileStream(filename, FileMode.Open, FileAccess.Read);
            }

            if (fp == null)
            {
                Console.WriteLine("Could not open file: %s\n", filename);
                return (int)DWG_ERROR.DWG_ERR_IOERROR;
            }

            //if (fp = null)
            //{
            //    fp == File.Create(Console.ReadLine());   
            //    //error = dat_read_stream(&bit_chain, fp);
            //    if (error >= (int)DWG_ERROR.DWG_ERR_CLASSESNOTFOUND)
            //        return error;
            //}
            //else
            {
                bit_chain.size = (new FileInfo(filename).Length);
                byte[] filebyte = null;
                error = dat_read_file(ref bit_chain, ref filebyte, filename);
                if (error >= (int)DWG_ERROR.DWG_ERR_CLASSESNOTFOUND)
                    return error;
            }
            fp.Close();

            decode decodeobj = new decode();
            error = decodeobj.dwg_decode(ref bit_chain, ref dwg);
            if (error >= (int)DWG_ERROR.DWG_ERR_CLASSESNOTFOUND)
            {
                Console.WriteLine("Failed to decode file: %s 0x%x\n", filename, error);
                //free(bit_chain.chain);
                bit_chain.chain = null;
                bit_chain.size = 0;
                return error;
            }



            return 1;
        }
        
    }
}
