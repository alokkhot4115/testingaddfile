﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWGApplication.Source
{
    public static class commonvar
    {
        public static readonly string[] version_codes =
        {
            "INVALID",
            "MC0.0",
            "AC1.2",
            "AC1.4",
            "AC1.50",
            "AC2.10",
            "AC1002",
            "AC1003",
            "AC1004",
            "AC1006",
            "AC1009",
            "AC1012",
            "AC1014",
            "AC1015",
            "AC1018",
            "AC1021",
            "AC1024",
            "AC1027",
            "AC1032",
            "------"
        };

        public static char[] readbytes(int startindex , int endindex , ref byte[] bytes)
        {
            byte[] readbytes = new Byte[Math.Abs(endindex - startindex)];
            if(readbytes.Length > 0)
            {
                for (int i = startindex; i < endindex; i++)
                {
                    readbytes[i] = bytes[i];
                }

                return Encoding.ASCII.GetChars(readbytes);
            }
            return null;            
        }
    };      

    class common
    {
    }

};

