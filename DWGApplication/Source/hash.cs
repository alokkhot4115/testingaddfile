﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWGApplication.Source
{
    
    public struct _hashbucket
    {
        public UInt32 key;
        public UInt32 value;
    };

    public struct dwg_inthash
    {
        public _hashbucket array; /* of key, value pairs */
        public UInt32 size;
        public UInt32 elems; // to get the fill rate
    };

    public class hash
    {
        public void hash_new(ref dwg_inthash ohash, UInt32 size)
        {
            ohash = new dwg_inthash();
            UInt32 cap;
            
            // multiply with load factor,
            // and round size to next power of 2 (fast) or prime (secure),
            if (size < 15)
                size = 15;
            cap = (UInt32)(size * 100.0 / 75);
            // this is slow, but only done once. clz would be much faster
            while (size <= cap)
                size <<= (int)1U;
            ohash.array = new _hashbucket(); // key+value pairs
            ohash.elems = 0;
            ohash.size = size; 
        }
    }
}
