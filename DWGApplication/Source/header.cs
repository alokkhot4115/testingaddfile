﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWGApplication.Source
{
    class header
    {
        public void headerread(ref Bit_Chain dat, ref Dwg_Data dwg)
        {

            if(out_json.IS_JSON)
            {

            }
            else
            {
                dwg.header.zero_5 = new Byte[5];
                for (int i = 0; i < 5; i++)
                {
                    dwg.header.zero_5[i] = dec_macros.bit_read_RC(ref dat);
                }

                dwg.header.is_maint = dec_macros.bit_read_RC(ref dat);


            }
        }
    }
}
