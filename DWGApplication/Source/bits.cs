﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWGApplication.Source
{
    public struct Bit_Chain
    {
        public byte[] chain;
        public long size;
        public long bytte;
        public byte bit;
        public char opts; // from dwg->opts, see DWG_OPTS_*
        public FileStream fh;
        public DWG_VERSION_TYPE version;
        public DWG_VERSION_TYPE from_version;
    };

    class bits
    {
        public static int loglevel = 0;
    }
}
