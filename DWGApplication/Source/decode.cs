﻿using DWGApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DWGApplication.Dwg_Data;

namespace DWGApplication.Source
{
    public class decode
    {
        public static int loglevel = 0;
        public static int cur_ver = 0;

        public static int decode_preR13(ref Bit_Chain dat, ref Dwg_Data dwg)
        {
            return 0;
        }

        public static int decode_R13_R2000(ref Bit_Chain dat, ref Dwg_Data dwg)
        {
            return 0;
        }

        public static int decode_R2004(ref Bit_Chain dat, ref Dwg_Data dwg)
        {
            int j, error = 0;
            _dwg_section section = new _dwg_section();
            {
                Dwg_Header _obj = dwg.header;
                Dwg_Object obj;
                Bit_Chain hdl_dat = dat;
                int i;

                dat.bytte = 0x06;

                // clang-format off
                header headerobj = new header();
                headerobj.headerread(ref dat, ref dwg);
                // clang-format on
              }
            return 0;
        }

        public static int decode_R2007(ref Bit_Chain dat, ref Dwg_Data dwg)
        {
            return 0;
        }
        
        public int dwg_decode(ref Bit_Chain dat, ref Dwg_Data dwg)
        {
            int i;
            char[] version = new char[7];

            dwg.num_object_refs = 0;
            // dwg->num_layers = 0; // see now dwg->layer_control->num_entries
            dwg.num_entities = 0;
            dwg.num_objects = 0;
            dwg.num_classes = 0;
            dwg.thumbnail.size = 0;
            dwg.thumbnail.chain = String.Empty;
            dwg.header.num_sections = 0;
            dwg.header.section_infohdr.num_desc = 0;
            //dwg.dwg_class = null;
            //dwg.object_ref = NULL;
            //dwg.object = NULL;
            hash hashobj = new hash();
            UInt32 hashu32 = (UInt32)(dat.size / 1000);
            hashobj.hash_new(ref dwg.object_map, hashu32);
            dwg.dirty_refs = 1;

            if (dwg.opts == 1)
            {
                loglevel = dwg.opts & 0xf;
                dat.opts = (char)dwg.opts;
            }

            dat.bytte = 0;
            dat.bit = 0;          

            version = commonvar.readbytes(0, 7, ref dat.chain);
            version[6] = '\0';

            dwg.header.from_version = 0;
            for (i = 0; i < (int)DWG_VERSION_TYPE.R_AFTER; i++)
            {
                string s = new string(version);
                string t = (commonvar.version_codes[i]);
                if (string.Compare(s, t) == 0)
                {
                    dwg.header.from_version = (DWG_VERSION_TYPE)i;
                    break;
                }
            }
            if (dwg.header.from_version < DWG_VERSION_TYPE.R_INVALID)
            {
                string s = new string(version);
                if (string.Compare(s, 2, "AC", 2, 2) == 0) // let's ignore MC0.0 for now
                {
                    Console.WriteLine("Invalid DWG, magic: %s", version);
                }
                else
                {
                    Console.WriteLine("Invalid or unimplemented DWG version code %s", version);
                }
                return (int)DWG_ERROR.DWG_ERR_INVALIDDWG;
            }
            dat.from_version = dwg.header.from_version;
            if (dwg.header.version != DWG_VERSION_TYPE.R_INVALID) // target version not set
            {
                dat.version = dwg.header.version = dat.from_version;
            }

            if(dec_macros.PRE(((int)DWG_VERSION_TYPE.R_13), dat))
            {
                Console.WriteLine("We don't decode many entities and no blocks yet.");
                if(config.IS_RELEASE)
                    return decode_preR13(ref dat, ref dwg);                
            }

            if(dec_macros.VERSIONS((int)DWG_VERSION_TYPE.R_13, (int)DWG_VERSION_TYPE.R_2000,dat)) 
                return decode_R13_R2000(ref dat, ref dwg);

            if (dec_macros.VERSION((int)DWG_VERSION_TYPE.R_2004,dat))
                return decode_R2004(ref dat, ref dwg);

            if (dec_macros.VERSION((int)DWG_VERSION_TYPE.R_2007,dat))
                return decode_R2007(ref dat, ref dwg);

            if(dec_macros.SINCE((int)DWG_VERSION_TYPE.R_2010, dat))
            {
                decode_r2007.read_r2007_init(ref dwg);
                return decode_R2004(ref dat,ref dwg);                
            }
                 


            return 1;
        }
    }
}
