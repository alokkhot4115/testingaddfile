﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DWGApplication.Source
{
    class dec_macros
    {
        public static bool PRE(int version, Bit_Chain dat)
        {
            DWGApplication.Source.decode.cur_ver = version;            
            if (((int)dat.from_version) < version)
                return true;

            return false;
        }

        public static bool VERSIONS(int v1, int v2, Bit_Chain dat)
        {
            DWGApplication.Source.decode.cur_ver = v1;
            if (((int)dat.from_version) >= v1 && ((int)dat.from_version) <= v2)
                return true;

            return false;
        }

        public static bool VERSION(int version , Bit_Chain dat)
        {
            DWGApplication.Source.decode.cur_ver = version;
            if ((int)dat.from_version == version)
                return true;

            return false;
        }

        public static bool SINCE(int version, Bit_Chain dat)
        {
            DWGApplication.Source.decode.cur_ver = version;
            if ((int)dat.from_version >= version)
                return true;

            return false;
        }

        public static long bit_position(ref Bit_Chain dat)
        {
            return (dat.bytte* 8) + (dat.bit & 7);
        }

        public static void bit_advance_position(ref Bit_Chain dat, long advance)
        {
            long pos = bit_position(ref dat); // do not change value in code
            long endpos = dat.size * 8;   // do not change value in code 

            long bitts = (long)dat.bit + advance;
            if (pos + advance > endpos)
            {
                bits.loglevel = dat.opts & 0xf;
                Console.WriteLine("Buffer overflow at pos %lu.%u, size %lu, advance by %ld",dat.bytte, dat.bit, dat.size, advance);
            }
            else if ((long)pos + advance < 0)
            {
                bits.loglevel = dat.opts & 0xf;
                Console.WriteLine("Buffer underflow at pos %lu.%u, size %lu, advance by %ld",
                           dat.bytte, dat.bit, dat.size, advance);
                dat.bytte = 0;
                dat.bit = 0;
                return;
            }

            dat.bytte += (bitts >> 3);
            dat.bit = (byte)(bitts & 7);
        }

        public static byte bit_read_RC(ref Bit_Chain dat)
        {
            byte result;
            byte bytte;

            //CHK_OVERFLOW(__FUNCTION__, 0)
            bytte = dat.chain[dat.bytte];
            if (dat.bit == 0)
                result = bytte;
            else
            {
                result = (byte)(((int)bytte) << ((int)dat.bit));
                if (dat.bytte < dat.size - 1)
                {
                    bytte = dat.chain[dat.bytte + 1];
                    result |= (byte)(bytte >> (8 - (int)dat.bit));
                }
                else
                {
                    bits.loglevel = dat.opts & 0xf;
                    Console.WriteLine("Buffer overflow at %lu", dat.bytte + 1);
                    return 0;
                }
            }

            bit_advance_position(ref dat, 8);
            return result;
        }
    }
}
