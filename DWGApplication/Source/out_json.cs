﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DWGApplication.Source
{
    public class out_json
    {
        public const bool IS_JSON = true;
        public string _path_field(string path)
        {
            int idx = path.LastIndexOf(']');
            string s = path.Substring(0, idx);
            if (s.Length > 0 && s[1] == '.')
            {
                return s[2].ToString();
            }
            return path;
        }

        public void HASH(ref Bit_Chain dat)
        {
            using (StreamWriter sw = new StreamWriter(dat.fh))
            {
                sw.WriteLine(string.Format("{\n"));                
            }
            dat.opts |= (char)0x20;                                                                 
            dat.bit++;                                                              
        }
        public void RECOND(ref Bit_Chain dat,string name)
        {
            PRINTFIRST(ref dat);
            _prefix(ref dat);
            using (StreamWriter sw = new StreamWriter(dat.fh))
            {
                sw.WriteLine(string.Format("\"%s\": " , _path_field(name)));
            }
            HASH(ref dat);              
        }

        public void KEY(ref Bit_Chain dat, string name)
        {
            PRINTFIRST(ref dat);
            _prefix(ref dat);
            using (StreamWriter sw = new StreamWriter(dat.fh))
            {
                sw.WriteLine(string.Format("\"%s\": ", _path_field(name)));
            }
        }

        public void ENDHASH(ref Bit_Chain dat)
        {
            using (StreamWriter sw = new StreamWriter(dat.fh))
            {
                sw.WriteLine(string.Format("\n"));
                dat.bit--;
                _prefix(ref dat);
                sw.WriteLine(string.Format("}"));
                //dat.opts &= (int)~0x20;
            }
        }

        public void ENDRECORD(ref Bit_Chain dat)
        {
            ENDHASH(ref dat);
        }

        public void PRINTFIRST(ref Bit_Chain dat)
        {
            int temp = dat.opts & 0x20;
            if (temp !=1 )
            {
                using (StreamWriter sw = new StreamWriter(dat.fh))
                {
                    sw.WriteLine(string.Format(",\n"));
                }                
            }
            //else                                                                     
            //    CLEARFIRST;
        }

        public void _prefix(ref Bit_Chain dat)
        {
            using (StreamWriter sw = new StreamWriter(dat.fh))
            {
                for (int _i = 0; _i < dat.bit; _i++)
                {
                    sw.WriteLine(string.Format("  "));
                }
            }               
        }

        public int dwg_write_json(ref Bit_Chain dat, ref Dwg_Data dwg)
        {            
            int minimal = dwg.opts & (0x10);  // dont change this value 
            Dwg_Data.Dwg_Header obj = dwg.header;
            int error = 0;

            using (StreamWriter sw = new StreamWriter("D:\\Repository\\Parts\\DWG_libre\\2018\\Arc111.json"))
            {
                //sw.WriteLine(string.Format("{\n  \"created_by\": \"%s\"","LibreDWG UNKNOWN"));                
                sw.Close();
            }

            dat.bit++; // ident

            if (minimal > 0)
            {
                json_fileheader_write(ref dat, ref dwg);
            }
            return 0;
        }

        public int json_fileheader_write(ref Bit_Chain dat, ref Dwg_Data dwg)
        {
            Dwg_Data.Dwg_Header _obj = dwg.header;
            Dwg_Object obj ;
            int i = 0;

            RECOND(ref dat, "FILEHEADER");            
            KEY(ref dat ,"version");
            using (StreamWriter sw = new StreamWriter(dat.fh))
            {
                sw.WriteLine(string.Format("\"%s\"", commonvar.version_codes[(int)dwg.header.version]));
            }
            
            // clang-format off
            //# include "header.spec"
            // clang-format on

            ENDRECORD(ref dat);
            return 0;
        }


    }
}
